\include "msjbofertorio-encabezado.inc"

\score {
	\new StaffGroup <<
		% force offset of colliding notes in chords:
		\override Score.NoteColumn #'force-hshift = #1.0
	\override Score.MetronomeMark #'padding = #8.0

	\include "msjbofertorio-acordes.inc"
	\include "msjbofertorio-flauta.inc"
	\include "msjbofertorio-tenor.inc"

	>> % notes

	\layout { }
} % score
